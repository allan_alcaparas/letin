<h3><a href="{{ route('categories.index') }}">View all categories</a></h3>

@if($category)

	<h2>{{ $category->name }}</h2>
	<p>Create at {{ $category->created_at }}</p>
@else

	404 not found


@endif