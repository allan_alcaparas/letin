<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Create Category</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
	
	<form action="/categories" method="POST">
		@csrf
		<label for="name">
			Name:
		</label>
		<input type="text" class="form form-control" name="name">
		<button type="submit" class="btn btn-success">Create New Category</button>

	</form>


</body>
</html>