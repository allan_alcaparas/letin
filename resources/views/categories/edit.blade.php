<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edit</title>

</head>
<body>

	<form action="{{ route('categories.show',['category'=>$category->id]) }}" method="POST">
		@method('PUT')
		@csrf

		<label for="name"> 
			Name:
		</label>
		<input type="text" class="form form-control" name="name" value="{{ $category->name }}">
		<button type="submit" class="btn btn-success">Update Category</button>
	</form>
</body>
</html>